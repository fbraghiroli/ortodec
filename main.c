#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <time.h>
#include <stdlib.h>
#include "pp_nrf24l01/piping_protocol.h"
#include "adcbr_protocol/adcbr_protocol.h"

#define HUM_L_NUM 16

#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#define dprintf(...) fprintf(stderr, __VA_ARGS__)

struct dec_conf {
	uint16_t ch_en;
	uint8_t human;
	uint8_t json_out;
};

/* Keep dec_conf global (piping protocol callback does not support a user data
 * pointer)
 */
static struct dec_conf conf;
static int sigterm_f = 0;
static struct adcbr_hum hum_l[HUM_L_NUM];

static void sig_handler(int sig)
{
	switch(sig) {
	case SIGTERM:
		sigterm_f = 1;
		break;
	}
}

static void print_head(struct pp_header *h)
{
	dprintf("%10s: 0x%x\n", "rcr", h->rcr);
	dprintf("%10s: %s\n", "ptype", h->ptype == PP_PTYPE_CTL ? "ctl":"radio");
	dprintf("%10s: %u\n", "psrc", h->psrc);
	dprintf("%10s: %02x %02x %02u %02u %02u\n", "addr",
		h->addr[0], h->addr[1], h->addr[2], h->addr[3], h->addr[4]);
}

static int data_cbk(struct pp_wdata *ppd, struct pp_header *h, void *d,
		    size_t dlen)
{
	int ret, i, j;
	time_t tt;
	struct tm* tm;

	time(&tt);
	tm = localtime(&tt);

	if (conf.human)
		print_head(h);

	if ((ret = adcbr_decode_hum(d, dlen, hum_l, HUM_L_NUM)) < 0) {
		dprintf("Payload error: %d\n", ret);
		return 0;
	}

	if (conf.human) {
		for (i = 0; i < ret; i++)
			if ((1 << hum_l[i].ch) & conf.ch_en)
				printf("ch[%u]: %u\n", hum_l[i].ch, hum_l[i].ad);
	} else if (conf.json_out) {
		printf("{");
		printf("\"t\": %04d%02d%02d%02d%02d%02d", tm->tm_year+1900,
		       tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min,
		       tm->tm_sec);
		for (i = 0; i < HUM_L_NUM; i++) {
			if (!((1 << i) & conf.ch_en))
				continue;
			for (j = 0; j < ret; j++)
				if (hum_l[j].ch == i)
					printf(", \"ch%02u\": %u", hum_l[j].ch,
					       hum_l[j].ad);
		}
		printf("}\n");

	} else {
		printf("%04d%02d%02d%02d%02d%02d,", tm->tm_year+1900,
		       tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min,
		       tm->tm_sec);
		for (i = 0; i < HUM_L_NUM; i++) {
			if (!((1 << i) & conf.ch_en))
				continue;
			for (j = 0; j < ret; j++)
				if (hum_l[j].ch == i)
					printf("%u,", hum_l[j].ad);
		}
		printf("\n");
	}
	fflush(stdout);
	return 0;
}

static int ctl_cbk(struct pp_wdata *ppd, struct pp_header *h, void *d,
		   size_t dlen)
{
	print_head(h);
	return 0;
}

int parse_args(int argc, char *argv[], struct dec_conf *c)
{
	int opt;
	char *strp;
	unsigned long int n;
	c->ch_en = 0;
	c->human = 0;

	while ((opt = getopt(argc, argv, "c:o:jh")) != -1) {
		switch (opt) {
		case 'c':
			if ((n = strtoul(optarg, &strp, 10)) > HUM_L_NUM-1 ||
			    *strp) {
				eprintf("Wrong channel: %.2s\n", optarg);
				return -1;
			}
			c->ch_en |= (1 << n);
			break;
		case 'j':
			c->json_out = 1;
			break;
	        case 'h':
			c->human = 1;
			break;
		case 'o':
			/* output file */
			break;
		default:
			eprintf("Usage: TODO\n");
			return -1;
		}
	}
	return 0;
}

int main(int argc, char *argv[])
{
	struct pp_wdata ppd;
	int rret, ret = 0;
	unsigned char buf_in[128];
	uint32_t frame_cnt = 0;

	if (parse_args(argc, argv, &conf) < 0)
		return -EINVAL;
	ppd.recv_d = data_cbk;
	ppd.recv_ctl = ctl_cbk;

	pp_init(&ppd);

	signal(SIGTERM, sig_handler);

	while(!sigterm_f) {
		rret = read(STDIN_FILENO, buf_in, 128-1);
		if (rret < 0) {
			eprintf("read err: %d", -errno);
			continue;
		} else if (rret == 0) {
			eprintf("eof\n");
			goto exit_ret;
		}

		if ((ret = pp_fetch(&ppd, buf_in, rret)) < 0) {
			eprintf("pp_fetch err: %d\n", ret);
			goto exit_ret;
		}
		if (ret && conf.human)
			dprintf("Got %d new frame(s)\n", ret);
		frame_cnt += ret;
	}

exit_ret:
	return ret;
}
