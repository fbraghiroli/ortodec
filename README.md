# Simple decoder for 'orto' project

It decodes piping protocol and adcbr protocol.

## Usage and examples

Basic usage (human mode):

```
nc 10.0.0.2 9999 | bin/ortodec -h
```

Output csv with timestamp and only ch0 and ch5:

```
nc 10.0.0.2 9999 | bin/ortodec -c 0 -c 5 > /tmp/log.csv
```

Plot csv data (timestamp + first two channels) with Gnuplot:
~~~~
set datafile separator ","
set timefmt ""
set xdata time
set timefmt "%Y%m%d%H%M%S"
set format x "%H:%M"
plot '/tmp/log.csv' using 1:2 with lines, '/tmp/log.csv' using 1:3 with lines
~~~~